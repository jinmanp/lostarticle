package com.manman.lostarticle.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.manman.lostarticle.FoundDetailActivity;
import com.manman.lostarticle.MainActivity;
import com.manman.lostarticle.R;
import com.manman.lostarticle.adapter.FoundArticleAdapter;
import com.manman.lostarticle.item.FoundArticleItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import javax.security.cert.X509Certificate;

public class FoundArticleFragment extends Fragment {

    private static final String SAVED_RECYCLER_VIEW_STATUS_ID = "savedRecyclerViewStatus";
    private static final String SAVED_RECYCLER_VIEW_DATASET_ID = "savedRecyclerViewDataset";

    private static FoundArticleAdapter adapter;
    private static View view;
    private static RecyclerView recyclerView;
    private ProgressBar foundArticleProgressBar;
    private View foundArticleProgressView;

    private static String startYmd;
    private static String endYmd;
    private static String prdtClCd01;
    private static String prdtClCd02;
    private static String lstLctCd;
    private static String searchNm;
    private static Boolean searchRegYmd;
    private static int pageNo;
    private static int pageSize;
    private static int totalCount;

    private static boolean myIsVisibleToUser;

    public FoundArticleFragment() {
        // Required empty public constructor
    }

    public static FoundArticleFragment newInstance(String startYmd, String endYmd, String prdtClCd01, String prdtClCd02, String lstLctCd, String searchNm, Boolean searchRegYmd) {
        FoundArticleFragment fragment = new FoundArticleFragment();
        Bundle bundle = new Bundle();
        bundle.putString("startYmd", startYmd);
        bundle.putString("endYmd", endYmd);
        bundle.putString("prdtClCd01", prdtClCd01);
        bundle.putString("prdtClCd02", prdtClCd02);
        bundle.putString("lstLctCd", lstLctCd);
        bundle.putString("searchNm", searchNm);
        bundle.putBoolean("searchRegYmd", searchRegYmd);
        fragment.setArguments(bundle);

        /*Log.d("newInstance startYmd", startYmd);
        Log.d("newInstance endYmd", endYmd);
        Log.d("newInstance prdtClCd01", prdtClCd01);
        Log.d("newInstance prdtClCd02", prdtClCd02);
        Log.d("newInstance lstLctCd", lstLctCd);
        Log.d("newInstance searchNm", searchNm);
        Log.d("newInstance searchRegYm", "" + searchRegYmd);*/

        return fragment;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        myIsVisibleToUser = isVisibleToUser;

        //Log.d("습득물 화면", "isVisibleToUser " + myIsVisibleToUser);

        if(isVisibleToUser && getActivity() != null){
            //화면에 실제로 보일때
            if(adapter != null){
                Log.d("습득물 화면", "setUserVisibleHint adapter not null");
                adapter.clear();
                recyclerView.scrollToPosition(0);
                selectFoundArticleList();
            }else{
                Log.d("습득물 화면", "setUserVisibleHint adapter null");
            }
        }else{
            //preload 될때(전페이지에 있을때)
            Log.d("습득물 화면", "preload");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("습득물 화면", "onCreate");

        if (savedInstanceState != null){

            Log.d("습득물 화면", "savedInstanceState not null");

            // getting recyclerview position
            Parcelable recyclerViewState = savedInstanceState.getParcelable(SAVED_RECYCLER_VIEW_STATUS_ID);
            // getting recyclerview items
            ArrayList<FoundArticleItem> listData = (ArrayList<FoundArticleItem>) savedInstanceState.getSerializable(SAVED_RECYCLER_VIEW_DATASET_ID);
            // Restoring adapter items
            adapter.addItems(listData);
            // Restoring recycler view position
            recyclerView.getLayoutManager().onRestoreInstanceState(recyclerViewState);
        }else{
            Log.d("습득물 화면", "savedInstanceState null");
            adapter = new FoundArticleAdapter(MainActivity.getAppContext());

            adapter.setItemClick(new FoundArticleAdapter.ItemClick() {
                @Override
                public void onClick(View view, int position) {
                    //Log.d("선택한 항목", "" + position);
                    FoundArticleItem selectedItem = adapter.getItem(position);

                    Intent intent = new Intent(getActivity(), FoundDetailActivity.class);
                    intent.putExtra("atcId", selectedItem.getAtcId());
                    intent.putExtra("fdFilePathImg", selectedItem.getFdFilePathImg());
                    intent.putExtra("fdPrdtNm", selectedItem.getFdPrdtNm());
                    intent.putExtra("fdYmd", selectedItem.getFdYmd());
                    intent.putExtra("fdHor", selectedItem.getFdHor());
                    intent.putExtra("fdLctNm", selectedItem.getFdLctNm());
                    intent.putExtra("fdPlace", selectedItem.getFdPlace());
                    intent.putExtra("prdtClNm", selectedItem.getPrdtClNm());
                    intent.putExtra("uniq", selectedItem.getUniq());
                    intent.putExtra("csteSteNm", selectedItem.getCsteSteNm());
                    intent.putExtra("depPlace", selectedItem.getDepPlace());
                    intent.putExtra("tel", selectedItem.getTel());
                    intent.putExtra("regYmd", selectedItem.getRegYmd());

                    startActivity(intent);
                }
            });
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d("습득물 화면", "onCreateView");

        if(getArguments() != null){

            Log.d("습득물 화면", "getArguments() != null");

            startYmd = getArguments().getString("startYmd");
            endYmd = getArguments().getString("endYmd");
            prdtClCd01 = getArguments().getString("prdtClCd01");
            prdtClCd02 = getArguments().getString("prdtClCd02");
            lstLctCd = getArguments().getString("lstLctCd");
            searchNm = getArguments().getString("searchNm");
            searchRegYmd = getArguments().getBoolean("searchRegYmd");

            /*Log.d("습득물 startYmd", startYmd);
            Log.d("습득물 endYmd", endYmd);
            Log.d("습득물 prdtClCd01", prdtClCd01);
            Log.d("습득물 prdtClCd02", prdtClCd02);
            Log.d("습득물 lstLctCd", lstLctCd);
            Log.d("습득물 searchNm", searchNm);
            Log.d("습득물 searchRegYmd", "" + searchRegYmd);*/
        }

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_found_article, container, false);

        recyclerView = view.findViewById(R.id.foundArticleRecyclerView);

        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);

        //스크롤 페이징 처리
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
                int itemTotalCount = recyclerView.getAdapter().getItemCount() - 1;

                if (lastVisibleItemPosition == itemTotalCount) {
                    Log.d("습득물 리스트", "리스트 마지막 스크롤!!");

                    int totalPageNo = (int) Math.ceil(totalCount / pageSize);

                    /*Log.d("습득물 리스트 totalPageNo", totalPageNo + "");
                    Log.d("습득물 리스트 pageNo", pageNo + "");
                    Log.d("습득물 리스트 getPackageName", MainActivity.getAppContext().getPackageName());*/

                    if (totalPageNo > pageNo) {

                        pageNo++;
                        //Log.d("습득물 리스트 pageNo++", pageNo + "");

                        String foundArticleParam = "";
                        foundArticleParam += "?url=/foundArticle/selectFoundArticleList"; //url
                        foundArticleParam += "&startYmd=" + startYmd; //검색시작일
                        foundArticleParam += "&endYmd=" + endYmd; //검색종료일
                        foundArticleParam += (prdtClCd01.equals("") || prdtClCd01 == null ? "" : "&prdtClCd01=" + prdtClCd01); //대분류
                        foundArticleParam += (prdtClCd02.equals("") || prdtClCd02 == null ? "" : "&prdtClCd02=" + prdtClCd02); //중분류
                        foundArticleParam += (lstLctCd.equals("") || lstLctCd == null ? "" : "&fdLctCd=" + lstLctCd); //분실지역코드
                        foundArticleParam += (searchNm.equals("") || searchNm == null ? "" : "&searchNm=" + searchNm); //물품명
                        foundArticleParam += "&searchRegYmd=" + searchRegYmd; //등록일 기준 조회 여부
                        foundArticleParam += "&pageNo=" + pageNo; //페이지 번호
                        foundArticleParam += "&p=" + MainActivity.getAppContext().getPackageName();

                        FoundArticleFragment.FoundArticleTask networkTask = new FoundArticleFragment.FoundArticleTask(view.getContext().getString(R.string.found_article_list_url), foundArticleParam);
                        networkTask.execute();
                    }

                }

            }

            /*@Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }*/

        });

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        Log.d("습득물 화면", "onSaveInstanceState");

        Parcelable recyclerview = recyclerView.getLayoutManager().onSaveInstanceState();
        // putting recyclerview position
        outState.putParcelable(SAVED_RECYCLER_VIEW_STATUS_ID, recyclerview);
        // putting recyclerview items
        outState.putSerializable(SAVED_RECYCLER_VIEW_DATASET_ID, adapter.getListData());

        super.onSaveInstanceState(outState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.d("습득물 화면", "onActivityCreated");
        setUserVisibleHint(myIsVisibleToUser);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("습득물 화면", "onViewCreated");
    }

    public void selectFoundArticleList() {

        Log.d("습득물 화면", "selectFoundArticleList");
        /*Log.d("습득물 화면", "startYmd : " + startYmd);
        Log.d("습득물 화면", "endYmd : " + endYmd);
        Log.d("습득물 화면", "prdtClCd01 : " + prdtClCd01);
        Log.d("습득물 화면", "prdtClCd02 : " + prdtClCd02);
        Log.d("습득물 화면", "lstLctCd : " + lstLctCd);
        Log.d("습득물 화면", "searchNm : " + searchNm);
        Log.d("습득물 화면", "searchRegYmd : " + searchRegYmd);*/

        pageNo = 1;

        String foundArticleParam = "";
        foundArticleParam += "?url=/foundArticle/selectFoundArticleList"; //url
        foundArticleParam += "&startYmd=" + startYmd; //검색시작일
        foundArticleParam += "&endYmd=" + endYmd; //검색종료일
        foundArticleParam += (prdtClCd01.equals("") || prdtClCd01 == null ? "" : "&prdtClCd01=" + prdtClCd01); //대분류
        foundArticleParam += (prdtClCd02.equals("") || prdtClCd02 == null ? "" : "&prdtClCd02=" + prdtClCd02); //중분류
        foundArticleParam += (lstLctCd.equals("") || lstLctCd == null ? "" : "&fdLctCd=" + lstLctCd); //분실지역코드
        foundArticleParam += (searchNm.equals("") || searchNm == null ? "" : "&searchNm=" + searchNm); //물품명
        foundArticleParam += "&searchRegYmd=" + searchRegYmd; //등록일 기준 조회 여부
        foundArticleParam += "&pageNo=" + pageNo; //페이지 번호
        foundArticleParam += "&p=" + MainActivity.getAppContext().getPackageName();

        // AsyncTask를 통해 HttpURLConnection 수행.
        FoundArticleFragment.FoundArticleTask networkTask = new FoundArticleFragment.FoundArticleTask(view.getContext().getString(R.string.found_article_list_url), foundArticleParam);
        networkTask.execute();

    }

    public class FoundArticleTask extends AsyncTask<Void, Void, Void> {

        private String url;
        private String param;

        public FoundArticleTask(String url, String param) {
            this.url = url;
            this.param = param;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            foundArticleProgressBar = MainActivity.getAppActivity().findViewById(R.id.progressBar);
            foundArticleProgressView = MainActivity.getAppActivity().findViewById(R.id.progressView);

            foundArticleProgressBar.setVisibility(View.VISIBLE);
            foundArticleProgressView.setVisibility(View.VISIBLE);
            MainActivity.getAppActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        }

        @Override
        protected Void doInBackground(Void... params) {

            try {

                //Log.d("습득물", "doInBackground");

                //Log.d("습득물", url + param);

                InputStream is;

                if(url.contains("https://")){

                    SSLContext sslContext = SSLContext.getInstance("SSL");
                    sslContext.init(null, new TrustManager[] {
                            new X509TrustManager() {
                                @Override
                                public void checkClientTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws CertificateException {

                                }

                                @Override
                                public void checkServerTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws CertificateException {

                                }

                                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                    return new java.security.cert.X509Certificate[0];
                                }
                            }
                    }, null);

                    /*
                    CertificateFactory cf = CertificateFactory.getInstance("X.509");

                    InputStream caInput = getResources().openRawResource(R.raw.maandoo);
                    Certificate ca = cf.generateCertificate(caInput);

                    // Create a KeyStore containing our trusted CAs
                    String keyStoreType = KeyStore.getDefaultType();
                    KeyStore keyStore = KeyStore.getInstance(keyStoreType);
                    keyStore.load(null,null);
                    keyStore.setCertificateEntry("ca", ca);

                    // Create a TrustManager that trusts the CAs in our KeyStore
                    String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
                    TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
                    tmf.init(keyStore);

                    // Create an SSLContext that uses our TrustManager
                    SSLContext sslContext = SSLContext.getInstance("TLS");

                    sslContext.init(null, tmf.getTrustManagers(), new java.security.SecureRandom());
                    caInput.close();
                    */

                    URL requestUrl = new URL(url + param);
                    HttpsURLConnection urlConnection = (HttpsURLConnection)requestUrl.openConnection();
                    urlConnection.setSSLSocketFactory(sslContext.getSocketFactory());
                    urlConnection.setDefaultUseCaches(false);
                    urlConnection.setDoInput(true);                         // 서버에서 읽기 모드 지정
                    urlConnection.setDoOutput(true);                       // 서버로 쓰기 모드 지정
                    urlConnection.setRequestMethod("POST");         // 전송 방식은 POST
                    urlConnection.setRequestProperty("content-type", "application/x-www-form-urlencoded");
                    is = urlConnection.getInputStream();

                }else{
                    URL requestUrl = new URL(url + param);
                    HttpURLConnection urlConnection = (HttpURLConnection) requestUrl.openConnection();
                    urlConnection.setDefaultUseCaches(false);
                    urlConnection.setDoInput(true);                         // 서버에서 읽기 모드 지정
                    urlConnection.setDoOutput(true);                       // 서버로 쓰기 모드 지정
                    urlConnection.setRequestMethod("POST");         // 전송 방식은 POST
                    urlConnection.setRequestProperty("content-type", "application/x-www-form-urlencoded");

                    is = urlConnection.getInputStream();
                    //is = requestUrl.openStream(); //url위치로 입력스트림 연결
                }

                BufferedReader br = new BufferedReader(new InputStreamReader(is));

                StringBuilder sb = new StringBuilder();
                String line;

                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

                br.close();

                String result = sb.toString().trim();

                //Log.d("습득물 result", result);

                JSONObject jsonObject = new JSONObject(result);
                totalCount = jsonObject.getInt("totalCount");
                pageNo = jsonObject.getInt("pageNo");
                pageSize = jsonObject.getInt("pageSize");

                if (totalCount == 0){
                    //Log.d("분실물", "결과 없음!!!");
                    MainActivity.showToast("검색 결과가 없습니다.");
                }else{

                    JSONArray resultList = jsonObject.getJSONArray("resultList");

                    for (int i = 0; i < resultList.length(); i++) {
                        FoundArticleItem foundArticleItem = new FoundArticleItem();
                        JSONObject data = resultList.getJSONObject(i);

                        foundArticleItem.setAtcId(data.getString("atcId"));
                        foundArticleItem.setCsteSteNm(data.getString("csteSteNm"));
                        foundArticleItem.setDepPlace(data.getString("depPlace"));
                        foundArticleItem.setFdFilePathImg(data.getString("fdFilePathImg"));
                        foundArticleItem.setFdHor(data.getString("fdHor"));
                        foundArticleItem.setFdLctNm(data.getString("fdLctNm"));
                        foundArticleItem.setFdLctCd(data.getString("fdLctCd"));
                        foundArticleItem.setFdPlace(data.getString("fdPlace"));
                        foundArticleItem.setFdPrdtNm(data.getString("fdPrdtNm"));
                        foundArticleItem.setFdSn(data.getString("fdSn"));
                        foundArticleItem.setFdYmd(data.getString("fdYmd"));
                        foundArticleItem.setFdKeepOrgn(data.getString("fndKeepOrgn"));
                        foundArticleItem.setOrgId(data.getString("orgId"));
                        foundArticleItem.setOrgNm(data.getString("orgNm"));
                        foundArticleItem.setPrdtClNm(data.getString("prdtClNm"));
                        foundArticleItem.setPrdtClCd01(data.getString("prdtClCd01"));
                        foundArticleItem.setPrdtClCd02(data.getString("prdtClCd02"));
                        foundArticleItem.setTel(data.getString("tel"));
                        foundArticleItem.setUniq(data.getString("uniq"));
                        foundArticleItem.setRegYmd(data.getString("regYmd"));

                        adapter.addItem(foundArticleItem);

                    }
                }

            } catch (Exception e) {
                //Toast.makeText(view.getContext(), "Parsing Error", Toast.LENGTH_SHORT).show();
                Log.e("doInBackground", e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void param) {

            try {

                //Log.d("습득물", "onPostExecute");

                adapter.notifyDataSetChanged();

                if (foundArticleProgressBar != null && foundArticleProgressView != null) {
                    foundArticleProgressBar.setVisibility(View.GONE);
                    foundArticleProgressView.setVisibility(View.GONE);
                    MainActivity.getAppActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }

                foundArticleProgressBar = null;


            } catch (Exception e) {
                Log.e("onPostExecute", "LostArticleTask" + e.toString());

                if (foundArticleProgressBar != null && foundArticleProgressView != null) {
                    foundArticleProgressBar.setVisibility(View.GONE);
                    foundArticleProgressView.setVisibility(View.GONE);
                    MainActivity.getAppActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }

                foundArticleProgressBar = null;
            }

            super.onPostExecute(param);
        }
    }

}
