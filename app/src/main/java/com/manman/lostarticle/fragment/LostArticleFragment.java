package com.manman.lostarticle.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.manman.lostarticle.LostDetailActivity;
import com.manman.lostarticle.MainActivity;
import com.manman.lostarticle.R;
import com.manman.lostarticle.adapter.LostArticleAdapter;
import com.manman.lostarticle.item.LostArticleItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public class LostArticleFragment extends Fragment {

    private static final String SAVED_RECYCLER_VIEW_STATUS_ID = "savedRecyclerViewStatus";
    private static final String SAVED_RECYCLER_VIEW_DATASET_ID = "savedRecyclerViewDataset";

    private static LostArticleAdapter adapter;
    private static View view;
    private static RecyclerView recyclerView;
    private ProgressBar lostArticleProgressBar;
    private View lostArticleProgressView;

    private static String startYmd;
    private static String endYmd;
    private static String prdtClCd01;
    private static String prdtClCd02;
    private static String lstLctCd;
    private static String searchNm;
    private static Boolean searchRegYmd;
    private static int pageNo;
    private static int pageSize;
    private static int totalCount;

    private static boolean myIsVisibleToUser;

    public LostArticleFragment() {
        // Required empty public constructor
    }

    public static LostArticleFragment newInstance(String startYmd, String endYmd, String prdtClCd01, String prdtClCd02, String lstLctCd, String searchNm, Boolean searchRegYmd) {
        LostArticleFragment fragment = new LostArticleFragment();
        Bundle bundle = new Bundle();
        bundle.putString("startYmd", startYmd);
        bundle.putString("endYmd", endYmd);
        bundle.putString("prdtClCd01", prdtClCd01);
        bundle.putString("prdtClCd02", prdtClCd02);
        bundle.putString("lstLctCd", lstLctCd);
        bundle.putString("searchNm", searchNm);
        bundle.putBoolean("searchRegYmd", searchRegYmd);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        myIsVisibleToUser = isVisibleToUser;

        //Log.d("분실물 화면", "isVisibleToUser " + myIsVisibleToUser);

        if(isVisibleToUser && getActivity() != null){

            //화면에 실제로 보일때
            if(adapter != null){
                Log.d("분실물 화면", "setUserVisibleHint adapter not null");
                adapter.clear();
                recyclerView.scrollToPosition(0);
                selectLostArticleList();
            }else{
                Log.d("분실물 화면", "setUserVisibleHint adapter null");
            }
        }else{
            //preload 될때(전페이지에 있을때)
            Log.d("분실물 화면", "preload");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("분실물 화면", "onCreate");

        if (savedInstanceState != null){

            Log.d("분실물 화면", "savedInstanceState not null");

            // getting recyclerview position
            Parcelable recyclerViewState = savedInstanceState.getParcelable(SAVED_RECYCLER_VIEW_STATUS_ID);
            // getting recyclerview items
            ArrayList<LostArticleItem> listData = (ArrayList<LostArticleItem>) savedInstanceState.getSerializable(SAVED_RECYCLER_VIEW_DATASET_ID);
            // Restoring adapter items
            adapter.addItems(listData);
            // Restoring recycler view position
            recyclerView.getLayoutManager().onRestoreInstanceState(recyclerViewState);
        }else{
            Log.d("분실물 화면", "savedInstanceState null");
            adapter = new LostArticleAdapter(MainActivity.getAppContext());

            adapter.setItemClick(new LostArticleAdapter.ItemClick() {
                @Override
                public void onClick(View view, int position) {
                    //Log.d("선택한 항목", "" + position);
                    LostArticleItem selectedItem = adapter.getItem(position);

                    Intent intent = new Intent(getActivity(), LostDetailActivity.class);
                    intent.putExtra("atcId", selectedItem.getAtcId());
                    intent.putExtra("lstFilePathImg", selectedItem.getLstFilePathImg());
                    intent.putExtra("lstPrdtNm", selectedItem.getLstPrdtNm());
                    intent.putExtra("lstYmd", selectedItem.getLstYmd());
                    intent.putExtra("lstHor", selectedItem.getLstHor());
                    intent.putExtra("lstLctNm", selectedItem.getLstLctNm());
                    intent.putExtra("lstPlace", selectedItem.getLstPlace());
                    intent.putExtra("prdtClNm", selectedItem.getPrdtClNm());
                    intent.putExtra("lstSteNm", selectedItem.getLstSteNm());
                    intent.putExtra("orgNm", selectedItem.getOrgNm());
                    intent.putExtra("tel", selectedItem.getTel());
                    intent.putExtra("regYmd", selectedItem.getRegYmd());

                    startActivity(intent);
                }
            });
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d("분실물 화면", "onCreateView");

        if(getArguments() != null){
            startYmd = getArguments().getString("startYmd");
            endYmd = getArguments().getString("endYmd");
            prdtClCd01 = getArguments().getString("prdtClCd01");
            prdtClCd02 = getArguments().getString("prdtClCd02");
            lstLctCd = getArguments().getString("lstLctCd");
            searchNm = getArguments().getString("searchNm");
            searchRegYmd = getArguments().getBoolean("searchRegYmd");
        }

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_lost_article, container, false);

        recyclerView = view.findViewById(R.id.lostArticleRecyclerView);

        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);

        //스크롤 페이징 처리
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
                int itemTotalCount = recyclerView.getAdapter().getItemCount() - 1;

                if (lastVisibleItemPosition == itemTotalCount) {
                    Log.d("분실물 리스트", "리스트 마지막 스크롤!!");

                    int totalPageNo = (int) Math.ceil(totalCount / pageSize);

                    //Log.d("분실물 리스트 totalPageNo", totalPageNo + "");
                    //Log.d("분실물 리스트 pageNo", pageNo + "");

                    if (totalPageNo > pageNo) {

                        pageNo++;
                        //Log.d("분실물 리스트 pageNo++", pageNo + "");

                        String lostArticleParam = "";
                        lostArticleParam += "?url=/lostArticle/selectLostArticleList"; //url
                        lostArticleParam += "&startYmd=" + startYmd; //검색시작일
                        lostArticleParam += "&endYmd=" + endYmd; //검색종료일
                        lostArticleParam += (prdtClCd01 == "" || prdtClCd01 == null ? "" : "&prdtClCd01=" + prdtClCd01); //대분류
                        lostArticleParam += (prdtClCd02 == "" || prdtClCd02 == null ? "" : "&prdtClCd02=" + prdtClCd02); //중분류
                        lostArticleParam += (lstLctCd == "" || lstLctCd == null ? "" : "&lstLctCd=" + lstLctCd); //분실지역코드
                        lostArticleParam += (searchNm == "" || searchNm == null ? "" : "&searchNm=" + searchNm); //물품명
                        lostArticleParam += "&searchRegYmd=" + searchRegYmd; //등록일 기준 조회 여부
                        lostArticleParam += "&pageNo=" + pageNo; //페이지 번호
                        lostArticleParam += "&p=" + MainActivity.getAppContext().getPackageName();

                        LostArticleTask networkTask = new LostArticleTask(view.getContext().getString(R.string.lost_article_list_url), lostArticleParam);
                        networkTask.execute();
                    }

                }

            }

            /*@Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }*/

        });

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        Log.d("분실물 화면", "onSaveInstanceState");

        Parcelable recyclerview = recyclerView.getLayoutManager().onSaveInstanceState();
        // putting recyclerview position
        outState.putParcelable(SAVED_RECYCLER_VIEW_STATUS_ID, recyclerview);
        // putting recyclerview items
        outState.putSerializable(SAVED_RECYCLER_VIEW_DATASET_ID, adapter.getListData());

        super.onSaveInstanceState(outState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.d("분실물 화면", "onActivityCreated");
        setUserVisibleHint(myIsVisibleToUser);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("분실물 화면", "onViewCreated");
    }

    public void selectLostArticleList() {

        Log.d("분실물 화면", "selectLostArticleList");
        //Log.d("분실물 화면", "startYmd : " + startYmd);
        //Log.d("분실물 화면", "endYmd : " + endYmd);

        pageNo = 1;

        String lostArticleParam = "";
        lostArticleParam += "?url=/lostArticle/selectLostArticleList"; //url
        lostArticleParam += "&startYmd=" + startYmd; //검색시작일
        lostArticleParam += "&endYmd=" + endYmd; //검색종료일
        lostArticleParam += (prdtClCd01 == "" || prdtClCd01 == null ? "" : "&prdtClCd01=" + prdtClCd01); //대분류
        lostArticleParam += (prdtClCd02 == "" || prdtClCd02 == null ? "" : "&prdtClCd02=" + prdtClCd02); //중분류
        lostArticleParam += (lstLctCd == "" || lstLctCd == null ? "" : "&lstLctCd=" + lstLctCd); //분실지역코드
        lostArticleParam += (searchNm == "" || searchNm == null ? "" : "&searchNm=" + searchNm); //물품명
        lostArticleParam += "&searchRegYmd=" + searchRegYmd; //등록일 기준 조회 여부
        lostArticleParam += "&pageNo=" + pageNo; //페이지 번호
        lostArticleParam += "&p=" + MainActivity.getAppContext().getPackageName();

        // AsyncTask를 통해 HttpURLConnection 수행.
        LostArticleTask networkTask = new LostArticleTask(view.getContext().getString(R.string.lost_article_list_url), lostArticleParam);
        networkTask.execute();

    }

    public class LostArticleTask extends AsyncTask<Void, Void, Void> {

        private String url;
        private String param;

        public LostArticleTask(String url, String param) {
            this.url = url;
            this.param = param;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            lostArticleProgressBar = MainActivity.getAppActivity().findViewById(R.id.progressBar);
            lostArticleProgressView = MainActivity.getAppActivity().findViewById(R.id.progressView);

            lostArticleProgressBar.setVisibility(View.VISIBLE);
            lostArticleProgressView.setVisibility(View.VISIBLE);
            MainActivity.getAppActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        }

        @Override
        protected Void doInBackground(Void... params) {

            try {

                InputStream is;

                if(url.contains("https://")){

                    SSLContext sslContext = SSLContext.getInstance("SSL");
                    sslContext.init(null, new TrustManager[] {
                            new X509TrustManager() {
                                @Override
                                public void checkClientTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws CertificateException {

                                }

                                @Override
                                public void checkServerTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws CertificateException {

                                }

                                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                    return new java.security.cert.X509Certificate[0];
                                }
                            }
                    }, null);

                    /*
                    CertificateFactory cf = CertificateFactory.getInstance("X.509");

                    InputStream caInput = getResources().openRawResource(R.raw.maandoo);
                    Certificate ca = cf.generateCertificate(caInput);

                    // Create a KeyStore containing our trusted CAs
                    String keyStoreType = KeyStore.getDefaultType();
                    KeyStore keyStore = KeyStore.getInstance(keyStoreType);
                    keyStore.load(null,null);
                    keyStore.setCertificateEntry("ca", ca);

                    // Create a TrustManager that trusts the CAs in our KeyStore
                    String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
                    TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
                    tmf.init(keyStore);

                    // Create an SSLContext that uses our TrustManager
                    SSLContext sslContext = SSLContext.getInstance("TLS");
                    sslContext.init(null, tmf.getTrustManagers(), new java.security.SecureRandom());
                    caInput.close();
                    */

                    URL requestUrl = new URL(url + param);
                    HttpsURLConnection urlConnection = (HttpsURLConnection)requestUrl.openConnection();
                    urlConnection.setSSLSocketFactory(sslContext.getSocketFactory());
                    urlConnection.setDefaultUseCaches(false);
                    urlConnection.setDoInput(true);                         // 서버에서 읽기 모드 지정
                    urlConnection.setDoOutput(true);                       // 서버로 쓰기 모드 지정
                    urlConnection.setRequestMethod("POST");         // 전송 방식은 POST
                    urlConnection.setRequestProperty("content-type", "application/x-www-form-urlencoded");
                    is = urlConnection.getInputStream();
                }else{
                    URL requestUrl = new URL(url + param);
                    HttpURLConnection urlConnection = (HttpURLConnection) requestUrl.openConnection();
                    urlConnection.setDefaultUseCaches(false);
                    urlConnection.setDoInput(true);                         // 서버에서 읽기 모드 지정
                    urlConnection.setDoOutput(true);                       // 서버로 쓰기 모드 지정
                    urlConnection.setRequestMethod("POST");         // 전송 방식은 POST
                    urlConnection.setRequestProperty("content-type", "application/x-www-form-urlencoded");

                    is = urlConnection.getInputStream();
                    //is = requestUrl.openStream(); //url위치로 입력스트림 연결
                }

                BufferedReader br = new BufferedReader(new InputStreamReader(is));

                StringBuilder sb = new StringBuilder();
                String line;

                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

                br.close();

                String result = sb.toString().trim();

                //Log.d("분실물 result", result);

                JSONObject jsonObject = new JSONObject(result);
                totalCount = jsonObject.getInt("totalCount");
                pageNo = jsonObject.getInt("pageNo");
                pageSize = jsonObject.getInt("pageSize");

                if (totalCount == 0){
                    //Log.d("분실물", "결과 없음!!!");
                    MainActivity.showToast("검색 결과가 없습니다.");
                }else{

                    JSONArray resultList = jsonObject.getJSONArray("resultList");

                    for (int i = 0; i < resultList.length(); i++) {
                        LostArticleItem lostArticleItem = new LostArticleItem();
                        JSONObject data = resultList.getJSONObject(i);

                        lostArticleItem.setAtcId(data.getString("atcId"));
                        lostArticleItem.setLstFilePathImg(data.getString("lstFilePathImg"));
                        lostArticleItem.setLstHor(data.getString("lstHor"));
                        lostArticleItem.setLstLctNm(data.getString("lstLctNm"));
                        lostArticleItem.setLstLctCd(data.getString("lstLctCd"));
                        lostArticleItem.setLstPlace(data.getString("lstPlace"));
                        lostArticleItem.setLstPlaceSeNm(data.getString("lstPlaceSeNm"));
                        lostArticleItem.setLstPrdtNm(data.getString("lstPrdtNm"));
                        lostArticleItem.setLstSbjt(data.getString("lstSbjt"));
                        lostArticleItem.setLstSteNm(data.getString("lstSteNm"));
                        lostArticleItem.setLstYmd(data.getString("lstYmd"));
                        lostArticleItem.setOrgId(data.getString("orgId"));
                        lostArticleItem.setOrgNm(data.getString("orgNm"));
                        lostArticleItem.setPrdtClNm(data.getString("prdtClNm"));
                        lostArticleItem.setPrdtClCd01(data.getString("prdtClCd01"));
                        lostArticleItem.setPrdtClCd02(data.getString("prdtClCd02"));
                        lostArticleItem.setTel(data.getString("tel"));
                        lostArticleItem.setUniq(data.getString("uniq"));
                        lostArticleItem.setRegYmd(data.getString("regYmd"));

                        adapter.addItem(lostArticleItem);

                    }
                }

            } catch (Exception e) {
                //Toast.makeText(view.getContext(), "Parsing Error", Toast.LENGTH_SHORT).show();
                Log.e("doInBackground", e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void param) {

            try {

                //Log.d("분실물", "onPostExecute");

                adapter.notifyDataSetChanged();

                if (lostArticleProgressBar != null && lostArticleProgressView != null) {
                    lostArticleProgressBar.setVisibility(View.GONE);
                    lostArticleProgressView.setVisibility(View.GONE);
                    MainActivity.getAppActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }

                lostArticleProgressBar = null;


            } catch (Exception e) {
                Log.e("onPostExecute", "LostArticleTask" + e.toString());

                if (lostArticleProgressBar != null && lostArticleProgressView != null) {
                    lostArticleProgressBar.setVisibility(View.GONE);
                    lostArticleProgressView.setVisibility(View.GONE);
                    MainActivity.getAppActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }

                lostArticleProgressBar = null;
            }

            super.onPostExecute(param);
        }
    }

}
