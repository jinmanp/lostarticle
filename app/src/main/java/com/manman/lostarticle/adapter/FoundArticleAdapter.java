package com.manman.lostarticle.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.manman.lostarticle.R;
import com.manman.lostarticle.item.FoundArticleItem;

import java.util.ArrayList;

public class FoundArticleAdapter extends RecyclerView.Adapter<FoundArticleAdapter.ItemViewHolder> {

    private Context context;

    // adapter에 들어갈 list 입니다.
    private ArrayList<FoundArticleItem> listData;

    //아이템 클릭시 실행 함수
    private ItemClick itemClick;

    public interface ItemClick {
        void onClick(View view, int position);
    }

    //아이템 클릭시 실행 함수 등록 함수
    public void setItemClick(ItemClick itemClick) {
        this.itemClick = itemClick;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
        }
    }

    public FoundArticleAdapter(Context context) {
        this.listData = new ArrayList<>();
        this.context = context;
    }

    @NonNull
    @Override
    public FoundArticleAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // LayoutInflater를 이용하여 전 단계에서 만들었던 item.xml을 inflate 시킵니다.
        // return 인자는 ViewHolder 입니다.
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_found_article, parent, false);
        return new FoundArticleAdapter.ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FoundArticleAdapter.ItemViewHolder holder, int position) {

        final int Position = position;

        // Item을 하나, 하나 보여주는(bind 되는) 함수입니다.
        holder.onBind(listData.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(itemClick != null){
                    itemClick.onClick(v, Position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        // RecyclerView의 총 개수 입니다.
        return listData.size();
    }

    public ArrayList<FoundArticleItem> getListData() {
        return listData;
    }

    public void addItems(ArrayList<FoundArticleItem> datas){
        this.listData = datas;
    }

    public void addItem(FoundArticleItem data) {
        // 외부에서 item을 추가시킬 함수입니다.
        listData.add(data);
    }

    public FoundArticleItem getItem(int position) {
        return listData.get(position);
    }

    public void clear(){
        this.listData.clear();
    }

    // RecyclerView의 핵심인 ViewHolder 입니다.
    // 여기서 subView를 setting 해줍니다.
    class ItemViewHolder extends RecyclerView.ViewHolder {

        private TextView fdPrdtNm;
        private TextView depPlace;
        private TextView prdtClNm;
        private TextView fdYmd;
        private ImageView fdFilePathImg;

        ItemViewHolder(View itemView) {
            super(itemView);

            fdPrdtNm = itemView.findViewById(R.id.fdPrdtNm);
            depPlace = itemView.findViewById(R.id.depPlace);
            prdtClNm = itemView.findViewById(R.id.fdPrdtClNm);
            fdYmd = itemView.findViewById(R.id.fdYmd);
            fdFilePathImg = itemView.findViewById(R.id.fdFilePathImg);
        }

        void onBind(FoundArticleItem data) {
            fdPrdtNm.setText(data.getFdPrdtNm());
            depPlace.setText(data.getDepPlace());
            prdtClNm.setText(data.getPrdtClNm());
            fdYmd.setText(data.getFdYmd());

            if(data.getFdFilePathImg().equals("https://www.lost112.go.kr/lostnfs/images/sub/img04_no_img.gif")){
                //Picasso.with(context).load(R.drawable.nophoto).into(fdFilePathImg);
                Glide.with(context).load(R.drawable.nophoto).into(fdFilePathImg);
            }else{
                //Picasso.with(context).load(data.getFdFilePathImg()).into(fdFilePathImg);
                Glide.with(context).load(data.getFdFilePathImg()).into(fdFilePathImg);
            }
        }
    }
}