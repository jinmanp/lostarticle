package com.manman.lostarticle.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.manman.lostarticle.fragment.FoundArticleFragment;
import com.manman.lostarticle.fragment.LostArticleFragment;

public class MainActivityAdapter extends FragmentStatePagerAdapter {

    private String startYmd;
    private String endYmd;
    private String prdtClCd01;
    private String prdtClCd02;
    private String lstLctCd;
    private String searchNm;
    private Boolean searchRegYmd;

    public MainActivityAdapter(FragmentManager fm, String startYmd, String endYmd, String prdtClCd01, String prdtClCd02, String lstLctCd, String searchNm, Boolean searchRegYmd) {
        super(fm);

        this.startYmd = startYmd;
        this.endYmd = endYmd;
        this.prdtClCd01 = prdtClCd01;
        this.prdtClCd02 = prdtClCd02;
        this.lstLctCd = lstLctCd;
        this.searchNm = searchNm;
        this.searchRegYmd = searchRegYmd;
    }

    @Override
    public Fragment getItem(int position) {

        /*Log.d("아답터 startYmd", startYmd);
        Log.d("아답터 endYmd", endYmd);
        Log.d("아답터 prdtClCd01", prdtClCd01);
        Log.d("아답터 prdtClCd02", prdtClCd02);
        Log.d("아답터 lstLctCd", lstLctCd);
        Log.d("아답터 searchNm", searchNm);
        Log.d("아답터 searchRegYmd", "" + searchRegYmd);*/

        switch(position)
        {
            case 0:
                return FoundArticleFragment.newInstance(startYmd, endYmd, prdtClCd01, prdtClCd02, lstLctCd, searchNm, searchRegYmd);
            case 1:
                return LostArticleFragment.newInstance(startYmd, endYmd, prdtClCd01, prdtClCd02, lstLctCd, searchNm, searchRegYmd);
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "습득물";
            case 1:
                return "분실물";
            default:
                return null;
        }
    }

}
