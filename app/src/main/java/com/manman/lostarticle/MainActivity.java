package com.manman.lostarticle;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.manman.lostarticle.adapter.MainActivityAdapter;
import com.manman.lostarticle.util.BackPressCloseHandler;
import com.manman.lostarticle.util.MyApplication;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private BackPressCloseHandler backPressCloseHandler;

    public static final int SEARCH_ACTIVITY = 1000;
    public static final int SETTING_ACTIVITY = 2000;

    private static Context context;
    private static Activity activity;

    private static ViewPager mainViewPager;
    private static MainActivityAdapter mainActivityAdapter;

    private String startYmd;
    private String endYmd;
    private String prdtClCd01;
    private String prdtClCd02;
    private String lstLctCd;
    private String searchNm;
    private Boolean searchRegYmd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        MainActivity.context = getApplicationContext();
        MainActivity.activity = MainActivity.this;

        setContentView(R.layout.activity_main);

        backPressCloseHandler = new BackPressCloseHandler(this);

        //Push 메시지에서 넘어온 값 있을 경우 세팅
        Intent intent = getIntent();
        if(intent.getExtras() != null){
            endYmd = intent.getExtras().getString("regYmd");
            startYmd = intent.getExtras().getString("regYmd");
            searchNm = "";
            prdtClCd01 = intent.getExtras().getString("selectedCategoryValue1");
            prdtClCd02 = intent.getExtras().getString("selectedCategoryValue2");
            lstLctCd = intent.getExtras().getString("selectedAreaValue");
            searchRegYmd = Boolean.parseBoolean(intent.getExtras().getString("searchRegYmd"));

            /*Log.d("메인화면 push", "prdtClCd01 : " + prdtClCd01);
            Log.d("메인화면 push", "prdtClCd02 : " + prdtClCd02);
            Log.d("메인화면 push", "lstLctCd : " + lstLctCd);
            Log.d("메인화면 push", "searchRegYmd : " + searchRegYmd);*/
        }else{

            //현재날짜
            Calendar cal = new GregorianCalendar(Locale.KOREA);
            cal.setTime(new Date());
            SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd");
            endYmd = fm.format(cal.getTime());

            //한달 전 날짜
            cal.add(Calendar.MONTH, -1);
            startYmd = fm.format(cal.getTime());
            searchNm = "";

            prdtClCd01 = "";
            prdtClCd02 = "";
            lstLctCd = "";
            searchRegYmd = false;
        }

        //상단 탭 삽입
        mainViewPager = findViewById(R.id.viewPager);
        mainActivityAdapter = new MainActivityAdapter(getSupportFragmentManager(), startYmd, endYmd, prdtClCd01, prdtClCd02, lstLctCd, searchNm, searchRegYmd);
        mainViewPager.setAdapter(mainActivityAdapter);

        TabLayout mainTabLayout = findViewById(R.id.tabs);
        mainTabLayout.setupWithViewPager(mainViewPager);

        mainViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mainTabLayout));

        // Set TabSelectedListener
        mainTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mainViewPager.setCurrentItem(tab.getPosition());
                //Log.d("탭클릭", "탭 : " + tab.getPosition());
                /*switch (tab.getPosition()) {
                    case 0:
                        foundArticleFragment = (FoundArticleFragment) getSupportFragmentManager().findFragmentByTag(
                                "android:switcher:" + mainViewPager.getId() + ":"
                                        + ((MainActivityAdapter) mainViewPager.getAdapter()).getItemId(0));
                        //foundArticleFragment.selectFoundArticleList();
                    case 1:
                        lostArticleFragment = (LostArticleFragment) getSupportFragmentManager().findFragmentByTag(
                                "android:switcher:" + mainViewPager.getId() + ":"
                                        + ((MainActivityAdapter) mainViewPager.getAdapter()).getItemId(1));
                        //lostArticleFragment.selectLostArticleList();
                    case 2:
                        infoFragment = (InfoFragment) getSupportFragmentManager().findFragmentByTag(
                                "android:switcher:" + mainViewPager.getId() + ":"
                                        + ((MainActivityAdapter) mainViewPager.getAdapter()).getItemId(2));

                }*/
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        // 앱 오픈 광고 삽입
        Application application = getApplication();
        ((MyApplication) application)
                .showAdIfAvailable(
                        MainActivity.this,
                        new MyApplication.OnShowAdCompleteListener() {
                            @Override
                            public void onShowAdComplete() {
                                Log.d("MainActivity", "onShowAdComplete");
                            }
                        });

        //배너광고 삽입
        adMob();

        //현재 등록 토큰 검색
        /*FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("MainActivity", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        Log.d("MainActivity", "token : " + token);
                    }
                });*/

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar_main_activity, menu) ;
        return true ;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search :
                Log.d("메인 화면", "검색 메뉴 클릭");

                Intent searchIntent = new Intent(getApplicationContext(), SearchActivity.class);
                searchIntent.putExtra("startYmd", startYmd);
                searchIntent.putExtra("endYmd", endYmd);
                searchIntent.putExtra("prdtClCd01", prdtClCd01);
                searchIntent.putExtra("prdtClCd02", prdtClCd02);
                searchIntent.putExtra("lstLctCd", lstLctCd);
                searchIntent.putExtra("currentItem", mainViewPager.getCurrentItem());
                searchIntent.putExtra("searchNm", searchNm);
                searchIntent.putExtra("searchRegYmd", searchRegYmd);

                startActivityForResult(searchIntent, SEARCH_ACTIVITY);

                return true;
            case R.id.action_setting :
                Log.d("메인 화면", "설정 메뉴 클릭");

                Intent settingIntent = new Intent(getApplicationContext(), SettingActivity.class);
                startActivityForResult(settingIntent, SETTING_ACTIVITY);

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        Log.d("메인 화면", "onSaveInstanceState");

        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case SEARCH_ACTIVITY:
                    startYmd = data.getStringExtra("startYmd");
                    endYmd = data.getStringExtra("endYmd");
                    prdtClCd01 = data.getStringExtra("prdtClCd01");
                    prdtClCd02 = data.getStringExtra("prdtClCd02");
                    lstLctCd = data.getStringExtra("lstLctCd");
                    searchNm = data.getStringExtra("searchNm");
                    searchRegYmd = data.getBooleanExtra("searchRegYmd", false);

                    /*Log.d("메인 화면", "startYmd : " + startYmd);
                    Log.d("메인 화면", "endYmd : " + endYmd);
                    Log.d("메인 화면", "prdtClCd01 : " + prdtClCd01);
                    Log.d("메인 화면", "prdtClCd02 : " + prdtClCd02);
                    Log.d("메인 화면", "lstLctCd : " + lstLctCd);
                    Log.d("메인 화면", "searchNm : " + searchNm);
                    Log.d("메인 화면", "searchRegYmd : " + searchRegYmd);*/

                    mainActivityAdapter = new MainActivityAdapter(getSupportFragmentManager(), startYmd, endYmd, prdtClCd01, prdtClCd02, lstLctCd, searchNm, searchRegYmd);
                    //mainActivityAdapter.notifyDataSetChanged();
                    mainViewPager.setAdapter(mainActivityAdapter);

                    mainViewPager.setCurrentItem(data.getIntExtra("currentItem", 0));

                    break;
                case SETTING_ACTIVITY:

                    Log.d("메인 화면", "SETTING_ACTIVITY onActivityResult");

                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        backPressCloseHandler.onBackPressed();
    }

    public static Context getAppContext() {
        return MainActivity.context;
    }

    public static Activity getAppActivity() {
        return MainActivity.activity;
    }

    private void adMob(){

        com.google.android.gms.ads.AdView mBannerAd;

        //Admob
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        AdView mAdView = findViewById(R.id.mainActivityAdView);

        //앱이 3세 이상 사용가능이라면 광고레벨을 설정해줘야 한다
        Bundle extras = new Bundle();
        extras.putString("max_ad_content_rating", "G");
        AdRequest adRequest = new AdRequest.Builder()
                .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                .build();

        //Admob
        mBannerAd = new AdView(this);
        mBannerAd.setAdSize(AdSize.LARGE_BANNER);
        mBannerAd.setAdUnitId(getString(R.string.banner_ad_found_detail_activity_id));

        mBannerAd.setAdListener(new AdListener() {

            @Override
            public void onAdClosed() {

            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
                Log.d("MainActivity","Admob mBannerAd onAdFailedToLoad : " + adError.getMessage());
                /*if (mNativeBannerAd.isAdLoaded()) {
                    View adView = NativeBannerAdView.render(MainActivity.this, mNativeBannerAd, NativeBannerAdView.Type.HEIGHT_100);
                    nativeBannerAdContainer.removeAllViews();
                    nativeBannerAdContainer.addView(adView);
                }*/
            }
            @Override
            public void onAdOpened() {

            }

            @Override
            public void onAdLoaded() {
                Log.d("MainActivity","Admob mBannerAd onAdLoaded");
                // Add the Native Banner Ad View to your ad container
                mAdView.removeAllViews();
                mAdView.addView(mBannerAd);
            }

            @Override
            public void onAdClicked() {

            }

        });

        //AdRequest adRequest = new AdRequest.Builder().build();
        mBannerAd.loadAd(adRequest);

    }

    public static void showToast(final String toast) {
        activity.runOnUiThread(() -> Toast.makeText(activity, toast, Toast.LENGTH_LONG).show());
    }
}
