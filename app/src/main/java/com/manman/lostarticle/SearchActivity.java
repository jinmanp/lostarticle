package com.manman.lostarticle;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class SearchActivity extends AppCompatActivity implements Button.OnClickListener {

    private RadioButton rgBtnRegYmd, rgBtnFdYmd;
    private Spinner category1, category2;
    private String selectedCategoryValue1, selectedCategoryValue2;
    private ArrayList<String> categoryValue1, categoryText1;
    private ArrayList<String> categoryValue2, categorySelectedValue2, categoryText2, categorySelectedText2;

    private int startYear, startMonth, startDay;
    private int endYear, endMonth, endDay;
    private TextView mStartDate, mEndDate;
    private String startDate, endDate;
    private EditText mSearchNm;

    private Spinner area;
    private String selectedAreaValue;
    private ArrayList<String> areaValue, areaText;
    private int currentItem;

    private String startYmd;
    private String endYmd;
    private String prdtClCd01;
    private String prdtClCd02;
    private String lstLctCd;
    private String searchNm;
    private Boolean searchRegYmd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        setContentView(R.layout.activity_search);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("검색");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        adMob();

        rgBtnRegYmd = findViewById(R.id.rg_btnRegYmd);
        rgBtnFdYmd = findViewById(R.id.rg_btnFdYmd);

        rgBtnRegYmd.setOnClickListener(optionOnClickListener);
        rgBtnFdYmd.setOnClickListener(optionOnClickListener);

        setCategory();

        category1 = findViewById(R.id.categorySpinner1);

        ArrayAdapter<String> adapter = new ArrayAdapter(this, R.layout.spinner_text_style, categoryText1);
        category1.setAdapter(adapter);

        category2 = findViewById(R.id.categorySpinner2);

        ArrayList<String> categoryTextTemp = new ArrayList<>();
        categoryTextTemp.add("전체");

        ArrayAdapter<String> adapter2 = new ArrayAdapter(this, R.layout.spinner_text_style, categoryTextTemp);
        category2.setAdapter(adapter2);

        mStartDate = findViewById(R.id.startDate);
        mEndDate = findViewById(R.id.endDate);
        mSearchNm = findViewById(R.id.editPrdtNm);

        ImageButton startDateBtn = findViewById(R.id.startDateBtn);
        startDateBtn.setOnClickListener(this) ;
        ImageButton endDateBtn = findViewById(R.id.endDateBtn);
        endDateBtn.setOnClickListener(this) ;
        Button searchBtn = findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(this);

        //현재 날짜와 시간을 가져오기위한 Calendar 인스턴스 선언
        Calendar cal = new GregorianCalendar();
        startYear = cal.get(Calendar.YEAR);
        startMonth = cal.get(Calendar.MONTH);
        startDay = cal.get(Calendar.DAY_OF_MONTH);

        endYear = cal.get(Calendar.YEAR);
        endMonth = cal.get(Calendar.MONTH);
        endDay = cal.get(Calendar.DAY_OF_MONTH);

        setCalendarNow("");
        setArea();

        area = findViewById(R.id.areaSpinner);
        ArrayAdapter<String> areaAdapter = new ArrayAdapter(this, R.layout.spinner_text_style, areaText);
        area.setAdapter(areaAdapter);

        //이전 화면에서 넘어온 값 있을 경우 세팅
        Intent intent = getIntent();
        startYmd = intent.getExtras().getString("startYmd");
        if(!(startYmd == null || startYmd.equals(""))){
            startYear = Integer.parseInt(startYmd.split("-")[0]);
            startMonth = Integer.parseInt(startYmd.split("-")[1]);
            startDay = Integer.parseInt(startYmd.split("-")[2]);
        }
        endYmd = intent.getExtras().getString("endYmd");
        if(!(endYmd == null || endYmd.equals(""))){
            endYear = Integer.parseInt(endYmd.split("-")[0]);
            endMonth = Integer.parseInt(endYmd.split("-")[1]);
            endDay = Integer.parseInt(endYmd.split("-")[2]);
        }
        prdtClCd01 = intent.getExtras().getString("prdtClCd01");
        prdtClCd02 = intent.getExtras().getString("prdtClCd02");
        lstLctCd = intent.getExtras().getString("lstLctCd");
        currentItem = intent.getExtras().getInt("currentItem");

        if(currentItem == 0){
            rgBtnFdYmd.setText("습득일자 기준");
        }else{
            rgBtnFdYmd.setText("분실일자 기준");
        }

        searchNm = intent.getExtras().getString("searchNm");

        if(!(searchNm == null || searchNm.equals(""))){
            mSearchNm.setText(searchNm);
        }

        searchRegYmd = intent.getExtras().getBoolean("searchRegYmd");

        if(searchRegYmd){
            rgBtnRegYmd.setChecked(true);
        }else{
            rgBtnFdYmd.setChecked(true);
        }

        category1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //Log.d("검색 화면", "분류1 선택 인덱스 : " + ""+i);
                selectedCategoryValue1 = categoryValue1.get(i);
                //Log.d("검색 화면", "분류1 선택 값 : " + selectedCategoryValue1);

                if(!(selectedCategoryValue1 == null || selectedCategoryValue1.equals(""))){

                    categorySelectedValue2 = new ArrayList<>();
                    categorySelectedText2 = new ArrayList<>();

                    //Log.d("검색 화면", "분류1 선택 분류 값 : " + selectedCategoryValue1.substring(0,3));

                    for(int o = 0; o < categoryValue2.size(); o++){
                        if(selectedCategoryValue1.substring(0,3).equals((categoryValue2.get(o).substring(0,3)))){
                            categorySelectedValue2.add(categoryValue2.get(o));
                            categorySelectedText2.add(categoryText2.get(o));
                        }
                    }

                    ArrayAdapter<String> selectedAdapter2 = new ArrayAdapter(getApplicationContext(), R.layout.spinner_text_style, categorySelectedText2);
                    category2.setAdapter(selectedAdapter2);

                    //Log.d("prdtClCd02Idx", "" + prdtClCd02Idx);
                    category2.setSelection(categorySelectedValue2.indexOf(prdtClCd02), true);

                }else{
                    ArrayList<String> categoryTextTemp = new ArrayList<>();
                    categoryTextTemp.add("전체");

                    ArrayAdapter<String> adapter2 = new ArrayAdapter(getApplicationContext(), R.layout.spinner_text_style, categoryTextTemp);
                    category2.setAdapter(adapter2);
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        category2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if(!(selectedCategoryValue1 == null || selectedCategoryValue1.equals(""))){
                    selectedCategoryValue2 = categorySelectedValue2.get(i);
                }else{
                    selectedCategoryValue2 = "";
                }

                //Log.d("검색 화면", "분류2 인덱스 : " + i);
                //Log.d("검색 화면", "분류2 선택 값 : " + selectedCategoryValue2);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedAreaValue = areaValue.get(i);
                //Log.d("검색 화면", "지역 선택 값 : " + selectedAreaValue);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d("검색 화면", "onResume");

        //Log.d("prdtClCd01Idx", "" + prdtClCd01Idx);
        category1.setSelection(categoryValue1.indexOf(prdtClCd01), true);

        if(!(startYmd == null || startYmd.equals(""))){
            mStartDate.setText(startYmd);
            startDate = startYmd;
        }

        if(!(endYmd == null || endYmd.equals(""))){
            mEndDate.setText(endYmd);
            endDate = endYmd;
        }

        if(!(lstLctCd == null || lstLctCd.equals(""))){
            //Log.d("lstLctCd", lstLctCd);
            //Log.d("area index", "" + areaValue.indexOf(lstLctCd));
            area.setSelection(areaValue.indexOf(lstLctCd), true);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar_search_activity, menu) ;
        return true ;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                Log.d("검색 화면", "뒤로 메뉴 클릭");
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.startDateBtn :
                Log.d("검색 화면", "달력 시작일 클릭");
                DatePickerDialog startDateDialog = new DatePickerDialog(SearchActivity.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, mStartDateSetListener, startYear,
                        startMonth-1, startDay);
                startDateDialog.getWindow().setGravity(Gravity.BOTTOM);
                startDateDialog.show();
                break ;
            case R.id.endDateBtn :
                Log.d("검색 화면", "달력 종료일 클릭");
                DatePickerDialog endDateDialog = new DatePickerDialog(SearchActivity.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, mEndDateSetListener, endYear,
                        endMonth-1, endDay);
                endDateDialog.getWindow().setGravity(Gravity.BOTTOM);
                endDateDialog.show();
                break ;
            case R.id.searchBtn :
                Log.d("검색 화면", "검색 버튼 클릭");
                Intent resultIntent = new Intent();
                resultIntent.putExtra("startYmd", startDate);
                resultIntent.putExtra("endYmd", endDate);
                resultIntent.putExtra("prdtClCd01", selectedCategoryValue1);
                resultIntent.putExtra("prdtClCd02", selectedCategoryValue2);
                resultIntent.putExtra("lstLctCd", selectedAreaValue);
                resultIntent.putExtra("currentItem", currentItem);
                resultIntent.putExtra("searchNm", mSearchNm.getText().toString());
                resultIntent.putExtra("searchRegYmd", searchRegYmd);
                setResult(RESULT_OK, resultIntent);
                finish();
                break ;
        }
    }

    //검색일 조건 선택 리스너
    RadioButton.OnClickListener optionOnClickListener
            = new RadioButton.OnClickListener() {

        public void onClick(View v) {

            searchRegYmd = rgBtnRegYmd.isChecked();
        }
    };

    //날짜 대화상자 리스너
    DatePickerDialog.OnDateSetListener mStartDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    //사용자가 입력한 값을 가져온뒤
                    startYear = year;
                    startMonth = monthOfYear;
                    startDay = dayOfMonth;

                    //텍스트뷰의 값을 업데이트함
                    setCalendarNow("startDate");
                }

            };

    DatePickerDialog.OnDateSetListener mEndDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    //사용자가 입력한 값을 가져온뒤
                    endYear = year;
                    endMonth = monthOfYear;
                    endDay = dayOfMonth;

                    //텍스트뷰의 값을 업데이트함
                    setCalendarNow("endDate");
                }

            };

    //날짜 텍스트뷰 업데이트
    void setCalendarNow(String target){

        switch(target){
            case "startDate":
                startMonth = startMonth + 1;
                mStartDate.setText(String.format("%d-%02d-%02d", startYear, startMonth, startDay));
                startDate = (String) mStartDate.getText();
                break;
            case "endDate":
                endMonth = endMonth + 1;
                mEndDate.setText(String.format("%d-%02d-%02d", endYear, endMonth, endDay));
                endDate = (String) mEndDate.getText();
                break;
             default:
                 startMonth = startMonth + 1;
                 endMonth = endMonth + 1;
                 mStartDate.setText(String.format("%d-%02d-%02d", startYear, startMonth, startDay));
                 mEndDate.setText(String.format("%d-%02d-%02d", endYear, endMonth, endDay));
                 startDate = (String) mStartDate.getText();
                 endDate = (String) mEndDate.getText();
                 break;
        }

    }

    void setCategory(){

        categoryValue1 = new ArrayList<>();
        categoryValue1.add("");
        categoryValue1.add("PRA000");
        categoryValue1.add("PRO000");
        categoryValue1.add("PRB000");
        categoryValue1.add("PRC000");
        categoryValue1.add("PRD000");
        categoryValue1.add("PRQ000");
        categoryValue1.add("PRE000");
        categoryValue1.add("PRR000");
        categoryValue1.add("PRM000");
        categoryValue1.add("PRK000");
        categoryValue1.add("PRF000");
        categoryValue1.add("PRG000");
        categoryValue1.add("PRH000");
        categoryValue1.add("PRN000");
        categoryValue1.add("PRI000");
        categoryValue1.add("PRP000");
        categoryValue1.add("PRL000");
        categoryValue1.add("PRJ000");
        categoryValue1.add("PRZ000");

        categoryText1 = new ArrayList<>();
        categoryText1.add("전체");
        categoryText1.add("가방");
        categoryText1.add("귀금속");
        categoryText1.add("도서용품");
        categoryText1.add("서류");
        categoryText1.add("산업용품");
        categoryText1.add("쇼핑백");
        categoryText1.add("스포츠용품");
        categoryText1.add("악기");
        categoryText1.add("유가증권");
        categoryText1.add("의류");
        categoryText1.add("자동차");
        categoryText1.add("전자기기");
        categoryText1.add("지갑");
        categoryText1.add("증명서");
        categoryText1.add("컴퓨터");
        categoryText1.add("카드");
        categoryText1.add("현금");
        categoryText1.add("휴대폰");
        categoryText1.add("기타물품");

        categoryValue2 = new ArrayList<>();
        categoryValue2.add("PRA100");
        categoryValue2.add("PRA200");
        categoryValue2.add("PRA300");
        categoryValue2.add("PRO100");
        categoryValue2.add("PRO200");
        categoryValue2.add("PRO300");
        categoryValue2.add("PRO400");
        categoryValue2.add("PRO500");
        categoryValue2.add("PRB100");
        categoryValue2.add("PRB200");
        categoryValue2.add("PRB300");
        categoryValue2.add("PRB400");
        categoryValue2.add("PRB500");
        categoryValue2.add("PRC100");
        categoryValue2.add("PRC200");
        categoryValue2.add("PRD100");
        categoryValue2.add("PRQ100");
        categoryValue2.add("PRE100");
        categoryValue2.add("PRR100");
        categoryValue2.add("PRR200");
        categoryValue2.add("PRR300");
        categoryValue2.add("PRR400");
        categoryValue2.add("PRR900");
        categoryValue2.add("PRM100");
        categoryValue2.add("PRM200");
        categoryValue2.add("PRM300");
        categoryValue2.add("PRM400");
        categoryValue2.add("PRK100");
        categoryValue2.add("PRK200");
        categoryValue2.add("PRK300");
        categoryValue2.add("PRK400");
        categoryValue2.add("PRF100");
        categoryValue2.add("PRF200");
        categoryValue2.add("PRF300");
        categoryValue2.add("PRF500");
        categoryValue2.add("PRF400");
        categoryValue2.add("PRG100");
        categoryValue2.add("PRG200");
        categoryValue2.add("PRG300");
        categoryValue2.add("PRG400");
        categoryValue2.add("PRG500");
        categoryValue2.add("PRG600");
        categoryValue2.add("PRH100");
        categoryValue2.add("PRH200");
        categoryValue2.add("PRH300");
        categoryValue2.add("PRN100");
        categoryValue2.add("PRN200");
        categoryValue2.add("PRN300");
        categoryValue2.add("PRN400");
        categoryValue2.add("PRI100");
        categoryValue2.add("PRI200");
        categoryValue2.add("PRI300");
        categoryValue2.add("PRI400");
        categoryValue2.add("PRI500");
        categoryValue2.add("PRP100");
        categoryValue2.add("PRP200");
        categoryValue2.add("PRP300");
        categoryValue2.add("PRL100");
        categoryValue2.add("PRL200");
        categoryValue2.add("PRL400");
        categoryValue2.add("PRL300");
        categoryValue2.add("PRJ100");
        categoryValue2.add("PRJ200");
        categoryValue2.add("PRJ400");
        categoryValue2.add("PRJ300");
        categoryValue2.add("PRJ600");
        categoryValue2.add("PRJ500");
        categoryValue2.add("PRZ100");

        categoryText2 = new ArrayList<>();
        categoryText2.add("여성용가방");
        categoryText2.add("남성용가방");
        categoryText2.add("기타");
        categoryText2.add("반지");
        categoryText2.add("목걸이");
        categoryText2.add("귀걸이");
        categoryText2.add("시계");
        categoryText2.add("기타");
        categoryText2.add("학습서적");
        categoryText2.add("소설");
        categoryText2.add("컴퓨터서적");
        categoryText2.add("만화책");
        categoryText2.add("기타");
        categoryText2.add("서류");
        categoryText2.add("기타");
        categoryText2.add("기타");
        categoryText2.add("쇼핑백");
        categoryText2.add("기타");
        categoryText2.add("건반악기");
        categoryText2.add("관악기");
        categoryText2.add("타악기");
        categoryText2.add("현악기");
        categoryText2.add("기타");
        categoryText2.add("어음");
        categoryText2.add("상품권");
        categoryText2.add("채권");
        categoryText2.add("기타");
        categoryText2.add("여성의류");
        categoryText2.add("남성의류");
        categoryText2.add("아기의류");
        categoryText2.add("기타");
        categoryText2.add("자동차열쇠");
        categoryText2.add("네비게이션");
        categoryText2.add("번호판");
        categoryText2.add("임시번호판");
        categoryText2.add("기타");
        categoryText2.add("PMP");
        categoryText2.add("MP3");
        categoryText2.add("PDA");
        categoryText2.add("카메라");
        categoryText2.add("전자수첩");
        categoryText2.add("기타");
        categoryText2.add("여성용지갑");
        categoryText2.add("남성용지갑");
        categoryText2.add("기타");
        categoryText2.add("신분증");
        categoryText2.add("면허증");
        categoryText2.add("여권");
        categoryText2.add("기타");
        categoryText2.add("삼성노트북");
        categoryText2.add("LG노트북");
        categoryText2.add("삼보노트북");
        categoryText2.add("HP노트북");
        categoryText2.add("기타");
        categoryText2.add("신용(체크)카드");
        categoryText2.add("일반카드");
        categoryText2.add("기타");
        categoryText2.add("현금");
        categoryText2.add("수표");
        categoryText2.add("외화");
        categoryText2.add("기타");
        categoryText2.add("삼성");
        categoryText2.add("LG");
        categoryText2.add("아이폰");
        categoryText2.add("스카이");
        categoryText2.add("모토로라");
        categoryText2.add("기타");
        categoryText2.add("기타");

    }

    void setArea(){
        areaValue = new ArrayList<>();
        areaValue.add("");
        areaValue.add("LCA000");
        areaValue.add("LCH000");
        areaValue.add("LCI000");
        areaValue.add("LCJ000");
        areaValue.add("LCK000");
        areaValue.add("LCQ000");
        areaValue.add("LCR000");
        areaValue.add("LCS000");
        areaValue.add("LCT000");
        areaValue.add("LCU000");
        areaValue.add("LCV000");
        areaValue.add("LCL000");
        areaValue.add("LCM000");
        areaValue.add("LCN000");
        areaValue.add("LCO000");
        areaValue.add("LCP000");
        areaValue.add("LCW000");
        areaValue.add("LCF000");
        areaValue.add("LCE000");

        areaText = new ArrayList<>();
        areaText.add("전체");
        areaText.add("서울특별시");
        areaText.add("강원도");
        areaText.add("경기도");
        areaText.add("경상남도");
        areaText.add("경상북도");
        areaText.add("광주광역시");
        areaText.add("대구광역시");
        areaText.add("대전광역시");
        areaText.add("부산광역시");
        areaText.add("울산광역시");
        areaText.add("인천광역시");
        areaText.add("전라남도");
        areaText.add("전라북도");
        areaText.add("충청남도");
        areaText.add("충청북도");
        areaText.add("제주도");
        areaText.add("세종특별자치시");
        areaText.add("해외");
        areaText.add("기타");

    }

    private void adMob(){

        com.google.android.gms.ads.AdView mBannerAd;

        //Admob
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        AdView mAdView = findViewById(R.id.searchActivityAdView);

        //앱이 3세 이상 사용가능이라면 광고레벨을 설정해줘야 한다
        Bundle extras = new Bundle();
        extras.putString("max_ad_content_rating", "G");
        AdRequest adRequest = new AdRequest.Builder()
                .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                .build();

        //Admob
        mBannerAd = new AdView(this);
        mBannerAd.setAdSize(AdSize.MEDIUM_RECTANGLE);
        mBannerAd.setAdUnitId(getString(R.string.banner_ad_found_detail_activity_id));

        mBannerAd.setAdListener(new AdListener() {

            @Override
            public void onAdClosed() {

            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
                Log.d("SearchActivity","Admob mBannerAd onAdFailedToLoad : " + adError.getMessage());
                /*if (mNativeBannerAd.isAdLoaded()) {
                    View adView = NativeBannerAdView.render(MainActivity.this, mNativeBannerAd, NativeBannerAdView.Type.HEIGHT_100);
                    nativeBannerAdContainer.removeAllViews();
                    nativeBannerAdContainer.addView(adView);
                }*/
            }
            @Override
            public void onAdOpened() {

            }

            @Override
            public void onAdLoaded() {
                Log.d("SearchActivity","Admob mBannerAd onAdLoaded");
                // Add the Native Banner Ad View to your ad container
                mAdView.removeAllViews();
                mAdView.addView(mBannerAd);
            }

            @Override
            public void onAdClicked() {

            }

        });

        //AdRequest adRequest = new AdRequest.Builder().build();
        mBannerAd.loadAd(adRequest);


    }

}
