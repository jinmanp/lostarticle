package com.manman.lostarticle;

import android.app.Application;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.os.CountDownTimer;
import android.util.Log;

import com.google.android.gms.ads.MobileAds;
import com.manman.lostarticle.util.MyApplication;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class SplashActivity extends AppCompatActivity {

    /*
    private com.google.android.gms.ads.InterstitialAd mInterstitialAd;
    //private com.facebook.ads.InterstitialAd interstitialAd;
    private static Handler mHandler;
    private int timer = 0;
    private boolean isAdShow = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        //Admob
        MobileAds.initialize(this, getString(R.string.admob_app_id));
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_main_activity_id));

        //앱이 3세 이상 사용가능이라면 광고레벨을 설정해줘야 한다
        Bundle extras = new Bundle();
        extras.putString("max_ad_content_rating", "G");
        AdRequest adRequest = new AdRequest.Builder()
                //.addNetworkExtrasBundle(FacebookAdapter.class, extras)
                .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                .build();

        mInterstitialAd.loadAd(adRequest);

        final Intent intent = new Intent(this, MainActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        //Push 메시지에서 넘어온 값 있을 경우 세팅
        Intent pushIntent = getIntent();
        if(pushIntent.getExtras() != null){
            String regYmd = pushIntent.getExtras().getString("regYmd");
            String selectedCategoryValue1 = pushIntent.getExtras().getString("selectedCategoryValue1");
            String selectedCategoryValue2 = pushIntent.getExtras().getString("selectedCategoryValue2");
            String selectedAreaValue = pushIntent.getExtras().getString("selectedAreaValue");
            String searchRegYmd = pushIntent.getExtras().getString("searchRegYmd");

            Log.d("SplashActivity push", "regYmd : " + regYmd);
            Log.d("SplashActivity push", "selectedCategoryValue1 : " + selectedCategoryValue1);
            Log.d("SplashActivity push", "selectedCategoryValue2 : " + selectedCategoryValue2);
            Log.d("SplashActivity push", "selectedAreaValue : " + selectedAreaValue);
            Log.d("SplashActivity push", "searchRegYmd : " + searchRegYmd);

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("selectedCategoryValue1", selectedCategoryValue1);
            intent.putExtra("selectedCategoryValue2", selectedCategoryValue2);
            intent.putExtra("selectedAreaValue", selectedAreaValue);
            intent.putExtra("regYmd", regYmd);
            intent.putExtra("searchRegYmd", searchRegYmd);
        }

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                //Log.d("SplashActivity","admob onAdLoaded");

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                //Log.d("SplashActivity","admob onAdFailedToLoad errorCode : " + errorCode);
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                //Log.d("SplashActivity","admob onAdClosed");
                startActivity(intent);
                finish();
            }
        });

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {

                if(timer == 6){
                    if(!isAdShow){
                        if (mInterstitialAd.isLoaded()) {
                            //Log.d("SplashActivity", "admob isLoaded mInterstitialAd.show()");
                            mInterstitialAd.show();
                            isAdShow = true;
                        }else{
                            Log.d("SplashActivity", "admob isNotLoaded startActivity(intent)");
                            isAdShow = true;
                            startActivity(intent);
                            finish();
                        }
                    }
                }else{
                    if(!isAdShow){
                        if (mInterstitialAd.isLoaded()) {
                            //Log.d("SplashActivity", "admob isLoaded mInterstitialAd.show()");
                            mInterstitialAd.show();
                            isAdShow = true;
                        }
                    }
                }

            }
        };

        class NewRunnable implements Runnable {

            @Override
            public void run() {
                while (true) {
                    //Log.d("SplashActivity", "timer : " + timer);

                    if(timer == 6){
                        break;
                    }else{
                        if(isAdShow){
                            break;
                        }
                    }

                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    timer = timer + 1;
                    mHandler.sendEmptyMessage(0);
                }
            }
        }

        NewRunnable nr = new NewRunnable();
        Thread t = new Thread(nr);
        t.start();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    */

    private static final String LOG_TAG = "SplashActivity";
    private final AtomicBoolean isMobileAdsInitializeCalled = new AtomicBoolean(false);

    private static final long COUNTER_TIME_MILLISECONDS = 5000;

    private long secondsRemaining;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        // 앱 오픈 광고 시작
        initializeMobileAdsSdk();

        final Intent intent = new Intent(this, MainActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        //Push 메시지에서 넘어온 값 있을 경우 세팅
        Intent pushIntent = getIntent();
        if(pushIntent.getExtras() != null){
            String regYmd = pushIntent.getExtras().getString("regYmd");
            String selectedCategoryValue1 = pushIntent.getExtras().getString("selectedCategoryValue1");
            String selectedCategoryValue2 = pushIntent.getExtras().getString("selectedCategoryValue2");
            String selectedAreaValue = pushIntent.getExtras().getString("selectedAreaValue");
            String searchRegYmd = pushIntent.getExtras().getString("searchRegYmd");

            Log.d("SplashActivity push", "regYmd : " + regYmd);
            Log.d("SplashActivity push", "selectedCategoryValue1 : " + selectedCategoryValue1);
            Log.d("SplashActivity push", "selectedCategoryValue2 : " + selectedCategoryValue2);
            Log.d("SplashActivity push", "selectedAreaValue : " + selectedAreaValue);
            Log.d("SplashActivity push", "searchRegYmd : " + searchRegYmd);

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("selectedCategoryValue1", selectedCategoryValue1);
            intent.putExtra("selectedCategoryValue2", selectedCategoryValue2);
            intent.putExtra("selectedAreaValue", selectedAreaValue);
            intent.putExtra("regYmd", regYmd);
            intent.putExtra("searchRegYmd", searchRegYmd);
        }

        // Create a timer so the SplashActivity will be displayed for a fixed amount of time.
        createTimer(COUNTER_TIME_MILLISECONDS, intent);

    }

    /**
     * Create the countdown timer, which counts down to zero and show the app open ad.
     *
     * @param time the number of milliseconds that the timer counts down from
     */
    private void createTimer(long time, Intent intent) {

        CountDownTimer countDownTimer =
                new CountDownTimer(time, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        secondsRemaining = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) + 1;
                        Log.d(LOG_TAG,
                                "App is done loading in: " + secondsRemaining);
                    }

                    @Override
                    public void onFinish() {
                        secondsRemaining = 0;
                        Log.d(LOG_TAG, "Done.");

                        startMainActivity(intent);

                    }
                };
        countDownTimer.start();
    }

    private void initializeMobileAdsSdk() {

        Log.d(LOG_TAG, "initializeMobileAdsSdk()");

        if (isMobileAdsInitializeCalled.getAndSet(true)) {
            return;
        }

        // Initialize the Mobile Ads SDK.
        MobileAds.initialize(this);

        // Load an ad.
        Application application = getApplication();
        ((MyApplication) application).loadAd(this);

    }

    /** Start the MainActivity. */
    public void startMainActivity(Intent intent) {
        ActivityCompat.finishAffinity(SplashActivity.this);
        startActivity(intent);
    }

}
