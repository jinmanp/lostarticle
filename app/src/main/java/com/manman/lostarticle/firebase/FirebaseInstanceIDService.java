package com.manman.lostarticle.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.manman.lostarticle.R;
import com.manman.lostarticle.SplashActivity;

import java.util.Objects;

public class FirebaseInstanceIDService extends FirebaseMessagingService {

    /**
     * 구글 토큰을 얻는 값입니다.
     * 아래 토큰은 앱이 설치된 디바이스에 대한 고유값으로 푸시를 보낼때 사용됩니다.
     * **/
    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        //Log.d("FirebaseMessage", "FirebaseInstanceIDService : " + s);
    }

    /**
     * 메세지를 받았을 경우 그 메세지에 대하여 구현하는 부분
     * **/
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {

        //Log.d("FirebaseMessage", "Message From: " + remoteMessage.getFrom());
        //Log.d("FirebaseMessage", "Message: " + remoteMessage);

        if (remoteMessage.getData().size() > 0) {
            sendNotification(remoteMessage);
        }
    }

    private void sendNotification(RemoteMessage remoteMessage) {

        String title = Objects.requireNonNull(remoteMessage.getNotification()).getTitle();
        String message = remoteMessage.getNotification().getBody();

        String selectedCategoryValue1 = remoteMessage.getData().get("selectedCategoryValue1");
        String selectedCategoryValue2 = remoteMessage.getData().get("selectedCategoryValue2");
        String selectedAreaValue = remoteMessage.getData().get("selectedAreaValue");
        String regYmd = remoteMessage.getData().get("regYmd");
        String searchRegYmd = remoteMessage.getData().get("searchRegYmd");

        /*Log.d("FirebaseMessage", "title : " + title);
        Log.d("FirebaseMessage", "message : " + message);
        Log.d("FirebaseMessage", "selectedCategoryValue1 : " + selectedCategoryValue1);
        Log.d("FirebaseMessage", "selectedCategoryValue2 : " + selectedCategoryValue2);
        Log.d("FirebaseMessage", "selectedAreaValue : " + selectedAreaValue);
        Log.d("FirebaseMessage", "regYmd : " + regYmd);*/

        Intent notifyIntent = new Intent(this, SplashActivity.class);
        notifyIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        notifyIntent.putExtra("selectedCategoryValue1", selectedCategoryValue1);
        notifyIntent.putExtra("selectedCategoryValue2", selectedCategoryValue2);
        notifyIntent.putExtra("selectedAreaValue", selectedAreaValue);
        notifyIntent.putExtra("regYmd", regYmd);
        notifyIntent.putExtra("searchRegYmd", searchRegYmd);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 9999, notifyIntent, PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE);

        String channel = "분실물창고";
        String channel_nm = "분실물창고";

        NotificationManager notichannel = (android.app.NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel channelMessage = new NotificationChannel(channel, channel_nm,
                android.app.NotificationManager.IMPORTANCE_DEFAULT);
        channelMessage.setDescription("분실물창고");
        channelMessage.enableLights(true);
        channelMessage.enableVibration(true);
        channelMessage.setShowBadge(false);
        //channelMessage.setVibrationPattern(new long[]{1000, 0, 0, 0});
        notichannel.createNotificationChannel(channelMessage);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channel)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setSmallIcon(R.drawable.ic_action_name)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setChannelId(channel)
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(9999, notificationBuilder.build());

    }
}