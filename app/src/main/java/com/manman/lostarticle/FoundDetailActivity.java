package com.manman.lostarticle;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

public class FoundDetailActivity extends AppCompatActivity {

    private com.google.android.gms.ads.AdView mBannerAd;
    //private InterstitialAd mInterstitialAd;
    //private Boolean isAdShow = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        setContentView(R.layout.activity_found_detail);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("습득물 상세정보");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        //Admob
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        //loadInterstitialAd();
        loadBannerAd();

        PhotoView ivFoundDetailFilePathImg = findViewById(R.id.foundDetailFilePathImg);
        TextView tvFoundDetailFdPrdtNm = findViewById(R.id.foundDetailFdPrdtNm);
        TextView tvFoundDetailFdYmd = findViewById(R.id.foundDetailFdYmd);
        TextView tvFoundDetailFdLctNm = findViewById(R.id.foundDetailFdLctNm);
        TextView tvFoundDetailFdPlace = findViewById(R.id.foundDetailFdPlace);
        TextView tvFoundDetailPrdtClNm = findViewById(R.id.foundDetailPrdtClNm);
        TextView tvFoundDetailUniq = findViewById(R.id.foundDetailUniq);
        TextView tvFoundDetailCsteSteNm = findViewById(R.id.foundDetailCsteSteNm);
        TextView tvFoundDetailDepPlace = findViewById(R.id.foundDetailDepPlace);
        TextView tvfoundDetailTel = findViewById(R.id.foundDetailTel);
        TextView tvfoundDetailRegYmd = findViewById(R.id.foundDetailRegYmd);

        Intent intent = getIntent();
        String atcId = intent.getExtras().getString("atcId");
        String fdFilePathImg = intent.getExtras().getString("fdFilePathImg");
        String fdPrdtNm = intent.getExtras().getString("fdPrdtNm");
        String fdYmd = intent.getExtras().getString("fdYmd");
        String fdHor = intent.getExtras().getString("fdHor");
        String fdLctNm = intent.getExtras().getString("fdLctNm");
        String fdPlace = intent.getExtras().getString("fdPlace");
        String prdtClNm = intent.getExtras().getString("prdtClNm");
        String uniq = intent.getExtras().getString("uniq");
        String csteSteNm = intent.getExtras().getString("csteSteNm");
        String depPlace = intent.getExtras().getString("depPlace");
        String tel = intent.getExtras().getString("tel");
        String regYmd = intent.getExtras().getString("regYmd");

        if(fdFilePathImg.equals("https://www.lost112.go.kr/lostnfs/images/sub/img04_no_img.gif")){
            ivFoundDetailFilePathImg.setVisibility(View.GONE);
        }else{
            //Picasso.with(getApplicationContext()).load(fdFilePathImg).into(ivFoundDetailFilePathImg);
            Glide.with(getApplicationContext()).load(fdFilePathImg).into(ivFoundDetailFilePathImg);
        }

        tvFoundDetailFdPrdtNm.setText(fdPrdtNm);
        tvFoundDetailFdYmd.setText(fdYmd + " " + fdHor + "시");
        tvFoundDetailFdLctNm.setText(fdLctNm);
        tvFoundDetailFdPlace.setText(fdPlace);
        tvFoundDetailPrdtClNm.setText(prdtClNm);
        tvFoundDetailUniq.setText(uniq);
        tvFoundDetailCsteSteNm.setText(csteSteNm);
        tvFoundDetailDepPlace.setText(depPlace);
        tvfoundDetailTel.setText(tel);
        tvfoundDetailRegYmd.setText(regYmd);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar_search_activity, menu) ;
        return true ;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                Log.d("습득물 상세 화면", "뒤로 메뉴 클릭");
                finish();

                /*if(!isAdShow) {
                    if(Math.random() < 0.65){
                        if (mInterstitialAd.isLoaded()) {
                            mInterstitialAd.show();
                            isAdShow = true;
                        }
                    }else{
                        finish();
                    }
                }else{
                    finish();
                }*/

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();

        /*if(!isAdShow) {
            if(Math.random() < 0.65){
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                    isAdShow = true;
                }
            }else{
                finish();
            }
        }else{
            finish();
        }*/

    }

    /*private void loadInterstitialAd() {

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_found_detail_activity_id));

        //앱이 3세 이상 사용가능이라면 광고레벨을 설정해줘야 한다
        Bundle extras = new Bundle();
        extras.putString("max_ad_content_rating", "G");
        final AdRequest adRequest = new AdRequest.Builder()
                .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                .build();

        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.d("FoundDetailActivity","interstitialAd onAdLoaded");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.d("FoundDetailActivity","interstitialAd onAdFailedToLoad");
                onBackPressed();
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                Log.d("FoundDetailActivity","interstitialAd onAdClosed");
                onBackPressed();
            }
        });

    }*/

    private void loadBannerAd(){

        AdView mAdView = findViewById(R.id.foundDetailActivityAdView);

        //앱이 3세 이상 사용가능이라면 광고레벨을 설정해줘야 한다
        Bundle extras = new Bundle();
        extras.putString("max_ad_content_rating", "G");
        AdRequest adRequest = new AdRequest.Builder()
                .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                .build();

        //Admob
        mBannerAd = new AdView(this);
        mBannerAd.setAdSize(AdSize.MEDIUM_RECTANGLE);
        mBannerAd.setAdUnitId(getString(R.string.banner_ad_found_detail_activity_id));

        mBannerAd.setAdListener(new AdListener() {

            @Override
            public void onAdClosed() {

            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
                Log.d("FoundDetailActivity","Admob mBannerAd onAdFailedToLoad : " + adError.getMessage());
                /*if (mNativeBannerAd.isAdLoaded()) {
                    View adView = NativeBannerAdView.render(MainActivity.this, mNativeBannerAd, NativeBannerAdView.Type.HEIGHT_100);
                    nativeBannerAdContainer.removeAllViews();
                    nativeBannerAdContainer.addView(adView);
                }*/
            }
            @Override
            public void onAdOpened() {

            }

            @Override
            public void onAdLoaded() {
                Log.d("FoundDetailActivity","Admob mBannerAd onAdLoaded");
                // Add the Native Banner Ad View to your ad container
                mAdView.removeAllViews();
                mAdView.addView(mBannerAd);
            }

            @Override
            public void onAdClicked() {

            }

        });

        //AdRequest adRequest = new AdRequest.Builder().build();
        mBannerAd.loadAd(adRequest);

    }

}
