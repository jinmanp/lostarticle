package com.manman.lostarticle.item;

import java.io.Serializable;

public class FoundArticleItem implements Serializable {

    private static final long serialVersionUID = 5825649609421945812L;

    private String atcId; //관리ID
    private String clrNm; //색상명
    private String depPlace; //보관장소
    private String fdFilePathImg; //습득물 사진 파일경로
    private String fdPrdtNm; //물품명
    private String fdSbjt; //게시제목
    private String fdSn; //습득순번
    private String fdYmd; //습득일자
    private String prdtClNm; //물품분류명
    private String rnum; //일련번호

    private String csteSteNm; //보관상태명
    private String fdHor; //습득시간
    private String fdLctNm; //
    private String fdLctCd; //
    private String fdPlace; //습득장소
    private String fdKeepOrgn; //습득물보관기관구분명
    private String prdtClCd01; //
    private String prdtClCd02; //
    private String orgId; //기관아이디
    private String orgNm; //기관명
    private String tel; //전화번호
    private String uniq; //특이사항
    private String regYmd; //등록일

    public String getAtcId() {
        return atcId;
    }

    public void setAtcId(String atcId) {
        this.atcId = atcId;
    }

    public String getClrNm() {
        return clrNm;
    }

    public void setClrNm(String clrNm) {
        this.clrNm = clrNm;
    }

    public String getDepPlace() {
        return depPlace;
    }

    public void setDepPlace(String depPlace) {
        this.depPlace = depPlace;
    }

    public String getFdFilePathImg() {
        return fdFilePathImg;
    }

    public void setFdFilePathImg(String fdFilePathImg) {
        this.fdFilePathImg = fdFilePathImg;
    }

    public String getFdPrdtNm() {
        return fdPrdtNm;
    }

    public void setFdPrdtNm(String fdPrdtNm) {
        this.fdPrdtNm = fdPrdtNm;
    }

    public String getFdSbjt() {
        return fdSbjt;
    }

    public void setFdSbjt(String fdSbjt) {
        this.fdSbjt = fdSbjt;
    }

    public String getFdSn() {
        return fdSn;
    }

    public void setFdSn(String fdSn) {
        this.fdSn = fdSn;
    }

    public String getFdYmd() {
        return fdYmd;
    }

    public void setFdYmd(String fdYmd) {
        this.fdYmd = fdYmd;
    }

    public String getPrdtClNm() {
        return prdtClNm;
    }

    public void setPrdtClNm(String prdtClNm) {
        this.prdtClNm = prdtClNm;
    }

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }

    public String getCsteSteNm() {
        return csteSteNm;
    }

    public void setCsteSteNm(String csteSteNm) {
        this.csteSteNm = csteSteNm;
    }

    public String getFdHor() {
        return fdHor;
    }

    public void setFdHor(String fdHor) {
        this.fdHor = fdHor;
    }

    public String getFdPlace() {
        return fdPlace;
    }

    public void setFdPlace(String fdPlace) {
        this.fdPlace = fdPlace;
    }

    public String getFdKeepOrgn() {
        return fdKeepOrgn;
    }

    public void setFdKeepOrgn(String fdKeepOrgn) {
        this.fdKeepOrgn = fdKeepOrgn;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgNm() {
        return orgNm;
    }

    public void setOrgNm(String orgNm) {
        this.orgNm = orgNm;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getUniq() {
        return uniq;
    }

    public void setUniq(String uniq) {
        this.uniq = uniq;
    }

    public String getFdLctNm() {
        return fdLctNm;
    }

    public void setFdLctNm(String fdLctNm) {
        this.fdLctNm = fdLctNm;
    }

    public String getFdLctCd() {
        return fdLctCd;
    }

    public void setFdLctCd(String fdLctCd) {
        this.fdLctCd = fdLctCd;
    }

    public String getPrdtClCd01() {
        return prdtClCd01;
    }

    public void setPrdtClCd01(String prdtClCd01) {
        this.prdtClCd01 = prdtClCd01;
    }

    public String getPrdtClCd02() {
        return prdtClCd02;
    }

    public void setPrdtClCd02(String prdtClCd02) {
        this.prdtClCd02 = prdtClCd02;
    }

    public String getRegYmd() {
        return regYmd;
    }

    public void setRegYmd(String regYmd) {
        this.regYmd = regYmd;
    }
}
