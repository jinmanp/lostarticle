package com.manman.lostarticle.item;

import java.io.Serializable;

public class LostArticleItem implements Serializable {

    private static final long serialVersionUID = -5622641232803434839L;

    private String atcId; //관리ID
    private String lstLctNm; //분실지역명
    private String lstPrdtNm; //분실물명
    private String lstSbjt; //게시제목
    private String lstYmd; //분실일자
    private String prdtClNm; //물품분류명
    private String rnum; //일련번호

    private String lstFilePathImg; //분실물이미지명
    private String lstHor; //분실시간
    private String lstPlace; //분실장소
    private String lstPlaceSeNm; //분실장소구분명
    private String lstSteNm; //분실물상태명
    private String orgId; //기관ID
    private String orgNm; //기관명
    private String tel; //기관전화번호
    private String uniq; //특이사항
    private String regYmd; //등록일
    private String lstLctCd;
    private String prdtClCd01;
    private String prdtClCd02;

    public String getAtcId() {
        return atcId;
    }

    public void setAtcId(String atcId) {
        this.atcId = atcId;
    }

    public String getLstFilePathImg() {
        return lstFilePathImg;
    }

    public void setLstFilePathImg(String lstFilePathImg) {
        this.lstFilePathImg = lstFilePathImg;
    }

    public String getLstHor() {
        return lstHor;
    }

    public void setLstHor(String lstHor) {
        this.lstHor = lstHor;
    }

    public String getLstLctNm() {
        return lstLctNm;
    }

    public void setLstLctNm(String lstLctNm) {
        this.lstLctNm = lstLctNm;
    }

    public String getLstPlace() {
        return lstPlace;
    }

    public void setLstPlace(String lstPlace) {
        this.lstPlace = lstPlace;
    }

    public String getLstPlaceSeNm() {
        return lstPlaceSeNm;
    }

    public void setLstPlaceSeNm(String lstPlaceSeNm) {
        this.lstPlaceSeNm = lstPlaceSeNm;
    }

    public String getLstPrdtNm() {
        return lstPrdtNm;
    }

    public void setLstPrdtNm(String lstPrdtNm) {
        this.lstPrdtNm = lstPrdtNm;
    }

    public String getLstSbjt() {
        return lstSbjt;
    }

    public void setLstSbjt(String lstSbjt) {
        this.lstSbjt = lstSbjt;
    }

    public String getLstSteNm() {
        return lstSteNm;
    }

    public void setLstSteNm(String lstSteNm) {
        this.lstSteNm = lstSteNm;
    }

    public String getLstYmd() {
        return lstYmd;
    }

    public void setLstYmd(String lstYmd) {
        this.lstYmd = lstYmd;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgNm() {
        return orgNm;
    }

    public void setOrgNm(String orgNm) {
        this.orgNm = orgNm;
    }

    public String getPrdtClNm() {
        return prdtClNm;
    }

    public void setPrdtClNm(String prdtClNm) {
        this.prdtClNm = prdtClNm;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getUniq() {
        return uniq;
    }

    public void setUniq(String uniq) {
        this.uniq = uniq;
    }

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }

    public String getLstLctCd() {
        return lstLctCd;
    }

    public void setLstLctCd(String lstLctCd) {
        this.lstLctCd = lstLctCd;
    }

    public String getPrdtClCd01() {
        return prdtClCd01;
    }

    public void setPrdtClCd01(String prdtClCd01) {
        this.prdtClCd01 = prdtClCd01;
    }

    public String getPrdtClCd02() {
        return prdtClCd02;
    }

    public void setPrdtClCd02(String prdtClCd02) {
        this.prdtClCd02 = prdtClCd02;
    }

    public String getRegYmd() {
        return regYmd;
    }

    public void setRegYmd(String regYmd) {
        this.regYmd = regYmd;
    }
}
